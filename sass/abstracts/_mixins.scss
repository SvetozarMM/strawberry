// Extends

%text-formating-300 {
    font-family: inherit;
    font-weight: 300; 
}
%animation-headings {
    animation-duration: .9s;
    backface-visibility: hidden;
}

// Mixins

/* Media Queries

min-width:

1. bp1400 
2. bp1500
3. bp1600
4. bp1700
5. bp1800

--------------------------------------------------------

max-width:

1. bp1100 [ 1100px / 16px = 68.75em ]
2. bp1000
3. bp900
4. bp800
5. bp768
6. bp700
7. bp640
8. bp600
9. bp550
10. bp500
11. bp450
12. bp400
13. bp350

--------------------------------------------------------

max-height:

1. bp450H
2. bp400H
3. bp320H

---------------------------------------------------------

Hover:

1. bpHoverNone 

---------------------------------------------------------

Responsive img:

1. smaller - 600px
2. small - 900px
3. mid - 1200px
4. big - 2000px
5. bigger - 3200px


*/

@mixin respond($breakpoint) {
    //max-width
    @if $breakpoint == bp1100 {
        @media only screen and (max-width: 68.75em) {
            @content           
        };
    }
    @if $breakpoint == bp1000 {
        @media only screen and (max-width: 62.5em) {
            @content           
        };
    }
    @if $breakpoint == bp900 {
        @media only screen and (max-width: 56.25em) {
            @content           
        };
    }
    @if $breakpoint == bp800 {
        @media only screen and (max-width: 50em) {
            @content           
        };
    }
    @if $breakpoint == bp768 {
        @media only screen and (max-width: 48em) {
            @content           
        };
    }
    @if $breakpoint == bp700 {
        @media only screen and (max-width: 43.75em) {
            @content           
        };
    }
    @if $breakpoint == bp640 {
        @media only screen and (max-width: 40em) {
            @content           
        };
    }
    @if $breakpoint == bp600 {
        @media only screen and (max-width: 37.5em) {
            @content           
        };
    }
    @if $breakpoint == bp550 {
        @media only screen and (max-width: 34.375em) {
            @content           
        };
    }
    @if $breakpoint == bp500 {
        @media only screen and (max-width: 31.25em) {
            @content           
        };
    }
    @if $breakpoint == bp450 {
        @media only screen and (max-width: 28.125em) {
            @content           
        };
    }
    @if $breakpoint == bp400 {
        @media only screen and (max-width: 25em) {
            @content           
        };
    }
    @if $breakpoint == bp350 {
        @media only screen and (max-width: 21.875em) {
            @content           
        };
    }
    //min-width
    @if $breakpoint == bp1400 {
        @media only screen and (min-width: 87.5em) {
            @content           
        };
    }
    @if $breakpoint == bp1500 {
        @media only screen and (min-width: 93.75em) {
            @content           
        };
    }
    @if $breakpoint == bp1600 {
        @media only screen and (min-width: 100em) {
            @content           
        };
    }
    @if $breakpoint == bp1700 {
        @media only screen and (min-width: 106.25em) {
            @content           
        };
    }
    @if $breakpoint == bp1800 {
        @media only screen and (min-width: 112.5em) {
            @content           
        };
    }
    //max-height
    @if $breakpoint == bp450H {
        @media only screen and (max-height: 28.125em) {
            @content           
        };
    }
    @if $breakpoint == bp400H {
        @media only screen and (max-height: 25em) {
            @content           
        };
    }
    @if $breakpoint == bp320H {
        @media only screen and (max-height: 20em) {
            @content           
        };
    }
    // Hover
    @if $breakpoint == bpHoverNone {
        @media only screen and (hover: none) {
            @content           
        };
    }
    // Responsive img
    @if $breakpoint == small {
        @media (min-resolution: 192dpi) and (min-width: 18.75em),
                (-webkit-min-device-pixel-ratio: 2) and (min-width: 18.75em),
                (min-width: 37.5em) {
                    @content 
        };                
    }
    @if $breakpoint == mid {
        @media (min-resolution: 192dpi) and (min-width: 28.125em),
                (-webkit-min-device-pixel-ratio: 2) and (min-width: 28.125em),
                (min-width: 56.25em) {
                    @content 
        };              
    }
    @if $breakpoint == big {
        @media (min-resolution: 192dpi) and (min-width: 37.5em),
                (-webkit-min-device-pixel-ratio: 2) and (min-width: 37.5em),
                (min-width: 75em) {
                    @content 
        };                
    }
    @if $breakpoint == bigger {
        @media (min-resolution: 192dpi) and (min-width: 62.5em),
                (-webkit-min-device-pixel-ratio: 2) and (min-width: 62.5em),
                (min-width: 125em) {
                    @content 
        };                
    }

}
